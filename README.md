# dp-homepage

#### 基本介绍
DP(Deepin Plain)本着专业极致地技术追求，搭建快速开发平台，实现可深度定制、敏捷开发的目标。在开发过程中遵循从开源中来到开源去的精神原则，汲取开源方案的优点，同时也结合自身对技术的理解和实践，从而获得更加完善的解决方案。

#### 开源项目
- dp-lte：[https://gitee.com/dp_group/dp-security/](https://gitee.com/dp_group/dp-security/)
- dp-pro：[https://gitee.com/dp_group/dp-pro](https://gitee.com/dp_group/dp-pro)
- dp-gen：[https://gitee.com/dp_group/dp-generator](https://gitee.com/dp_group/dp-generator)
- dp-boot：[https://gitee.com/dp_group/dp-boot](https://gitee.com/dp_group/dp-BOOT)
- dp-passport：[https://gitee.com/dp_group/dp-passport](https://gitee.com/dp_group/dp-passport)
- 项目文档：[http://dp-dev.mydoc.io/](http://dp-dev.mydoc.io/)